import numpy as np
import matplotlib.pyplot as plt
import pandas as pd
import os.path
from os import path
from keras.models import Sequential
from keras.layers import Dense
from keras.layers import LSTM
from keras.layers import Dropout
from datetime import datetime
from datetime import timedelta

# get data
dataset_train = pd.read_csv('data/USDJPY_20091221-000000_20200114-120000_240-train.csv')
training_set = dataset_train.iloc[:, 3:4].values # get values data CLOSE

# scale data to 0,1
from sklearn.preprocessing import MinMaxScaler
sc = MinMaxScaler(feature_range = (0, 1))
training_set_scaled = sc.fit_transform(training_set)

# make data train, X = 60 time steps, Y =  1 time step
X_train = []
y_train = []
no_of_sample = len(training_set)

for i in range(60, no_of_sample):
    X_train.append(training_set_scaled[i-60:i, 0])
    y_train.append(training_set_scaled[i, 0])

X_train, y_train = np.array(X_train), np.array(y_train)

X_train = np.reshape(X_train, (X_train.shape[0], X_train.shape[1], 1))


# build model LSTM
regressor = Sequential()
regressor.add(LSTM(units = 50, return_sequences = True, input_shape = (X_train.shape[1], 1)))
regressor.add(Dropout(0.2))
regressor.add(LSTM(units = 50, return_sequences = True))
regressor.add(Dropout(0.2))
regressor.add(LSTM(units = 50, return_sequences = True))
regressor.add(Dropout(0.2))
regressor.add(LSTM(units = 50))
regressor.add(Dropout(0.2))
regressor.add(Dense(units = 1))
regressor.compile(optimizer = 'adam', loss = 'mean_squared_error')

# if exists weight model then load file model thi load
if path.exists("mymodel_usdjpy.h5"):
    regressor.load_weights("mymodel_usdjpy.h5")
else:
    # else start train model
    regressor.fit(X_train, y_train, epochs = 10, batch_size = 32)
    regressor.save("mymodel_usdjpy.h5")

# Load data test 2/12/2019 - 14/1/2020
dataset_test = pd.read_csv('data/USDJPY_20091221-000000_20200114-120000_240-test.csv')
real_stock_price = dataset_test.iloc[:, 3:4].values

# prediction
dataset_total = pd.concat((dataset_train['Close'], dataset_test['Close']), axis = 0)
inputs = dataset_total[len(dataset_total) - len(dataset_test) - 60:].values
inputs = inputs.reshape(-1,1)
inputs = sc.transform(inputs)

X_test = []
no_of_sample = len(inputs)

for i in range(60, no_of_sample):
    X_test.append(inputs[i-60:i, 0])

X_test = np.array(X_test)
X_test = np.reshape(X_test, (X_test.shape[0], X_test.shape[1], 1))
predicted_stock_price = regressor.predict(X_test)
predicted_stock_price = sc.inverse_transform(predicted_stock_price)

# pylot real values and predict values
plt.plot(real_stock_price, color = 'red', label = 'Real values CLOSE USDJPY')
plt.plot(predicted_stock_price, color = 'blue', label = 'Predicted values CLOSE USDJPY')
plt.title('USDJPY values CLOSE Prediction')
plt.xlabel('Time')
plt.ylabel('USDJPY CLOSE values')
plt.legend()
plt.show()

# prediction the next day
lasttime = dataset_test['Date'][-1:]
print("OKOKOOK",lasttime)
m = int(str(lasttime).split(':')[1])
h = int(str(lasttime).split(':')[0].split('.')[2].split(" ")[1])
d = int(str(lasttime).split(':')[0].split('.')[2].split(" ")[0])
M = int(str(lasttime).split(':')[0].split('.')[1])
Y = int(str(lasttime).split(':')[0].split('.')[0].split(" ")[4])

# print('{}, {}, {}, {}, {}'.format(m,h,d,M,Y))

lasttime_ = datetime(Y, M, d, h, m)
print(lasttime_)
# B = lasttime_ + timedelta(hours=4*(i+1))

dataset_test = dataset_test['Close'][len(dataset_test)-60:len(dataset_test)].to_numpy()
dataset_test = np.array(dataset_test)

inputs = dataset_test
inputs = inputs.reshape(-1,1)
inputs = sc.transform(inputs)


num = 110
i = 0
while i< num:
    X_test = []
    no_of_sample = len(dataset_test)

    # get last data
    X_test.append(inputs[no_of_sample - 60:no_of_sample, 0])
    X_test = np.array(X_test)
    X_test = np.reshape(X_test, (X_test.shape[0], X_test.shape[1], 1))

    # prediction
    predicted_stock_price = regressor.predict(X_test)

    # scale data (0,1) to real values
    predicted_stock_price = sc.inverse_transform(predicted_stock_price)

    # add time
    dataset_test = np.append(dataset_test, predicted_stock_price[0], axis=0)
    inputs = dataset_test
    inputs = inputs.reshape(-1, 1)
    inputs = sc.transform(inputs)

    print('values CLOSE ' + str(lasttime_ + timedelta(hours=4*(i+1))) + ' of USDJPY : ', predicted_stock_price[0][0])
    i = i +1

