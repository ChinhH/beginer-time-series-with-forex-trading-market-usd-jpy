# Beginer Time Series with forex trading market USD JPY

>Lượt xem: 3,456

Chào tất cả anh em, hôm nay chúng ta sẽ cùng nhau tìm hiểu về bài toán time series. Cụ thể là chúng ta sẽ xây model dự đoán tỷ giá của Đô la Mỹ vs Đồng Yên Nhật.
Để xem năm vừa rồi và năm nay giá cả lên /xuống như nào mà lướt ít sóng kiếm tiền mua ôtô nhé.

Bài toán này mình sẽ áp dụng một mạng LSTM (Long Short Term Memory), một biến thể của Recurrent Neural Network (RNN) để dự đoán tỉ giá của Đô la Mỹ vs Yên Nhật.
Đầu vào là dữ liệu train là giá lịch sử (mình sẽ chọn giá đóng CLOSE của tỉ giá) từ 2009 đến tháng 11 năm 2019 (~10 năm).

Sau khi train chúng ta sẽ kiểm tra model xem có chính xác hay không bằng cách dự đoán tỉ giá của Đô la Mỹ vs Đồng yên Nhật trong tháng 12 năm 2019 và tháng 1 năm 2020 xem sao nhé.

![Recordit GIF](image/USDJPY-TES700.png)

>Warning: Be careful otherwise you will become rich

# Bước 1. Chuẩn bị dữ liệu

Dữ liệu mình đã chuẩn bị từ sàn "https://www.atfx.com/" bằng tool viết trên "MetaEditor 4", mình sẽ có bài hướng dẫn sau nếu các bạn có nhu cầu tìm hiểu về nó nhé.

Dữ liệu mình thu thập được từ năm 2009 đến ngày 14 tháng 1 năm 2020.
Mình sẽ chia dữ liệu thành 2 phần: 1 tập train lấy dữ liệu từ năm 2009 đến tháng 11 năm 2019. Phần còn lại là dữ liệu test từ tháng 12 năm 2019 đến ngày 14 tháng 1 năm 2020.

Timestep: ở đây mình thu thập mỗi timestep là 4h. Chúng ta hãy cùng xem qua dữ liệu

![Recordit GIF](image/datahead.PNG)

Ở đây chúng ta sẽ quan tâm đến giá trị Close tức là tỉ giá đóng.

# Bước 2. Timestep and scale data

Ta sẽ coi mỗi 4h là 1 timestep. Ta sẽ sử dụng 60 time steps làm input để đưa vào mạng train và đầu ra sẽ là time step tiếp theo.

Chúng ta sẽ dự đoán thử tỉ giá 1/12/2019 đến 14/1/2020 và so sánh giữa giá dự đoán và giá thực (đã có trong file dữ liệu) xem model của chúng ta dự đoán chuẩn không nhé.
Bước cuối, chúng ta sẽ dự đoán tỉ giá trong hết tháng 1/2020 (từ ngày 15 đến ngày 31) của tỉ giá USD JPY, các bạn kiểm chứng giúp xem có đúng ko nhé

![Recordit GIF](image/datascale.PNG)

Data ở đây đang có giá trị khá lớn, nên chúng ta cần scale data lại nằm trong khoảng -1 đến 1 hoặc 0 đến 1

# Bước 3. Build model

Mình sẽ xây dựng model với 4 lớp lstm và 1 lớp dense.
optimizer mình sửa dụng 'adam'(short for Adaptive Moment Estimation). Loss function 'mean_squared_error'

![Recordit GIF](image/model.PNG)

# Bước 4. Training model

![Recordit GIF](image/train.PNG)

# Bước 5. Test model

Get data test và load model

![Recordit GIF](image/loadtest.PNG)

vẽ đồ thị dữ liệu dự đoán được và dữ liệu thật

![Recordit GIF](image/pylottest.PNG)

Dự đoán những ngày tiếp cho đến hết tháng 1 năm 2020

![Recordit GIF](image/shownextday.PNG)

![Recordit GIF](image/shownextday2.PNG)

![Recordit GIF](image/shownextday3.PNG)

>Warning: Be careful otherwise you will become rich

# Bước 6. Time Series Forecasting Performance Measures

Tại bước 3 xây dựng model, chúng ta có sử dụng loss function "mean_squared_error"

Bây giờ chúng ta cùng tìm hiểu thêm một vài hàm tính toán loss cho bài toán time series mà các bạn có thể tham khảo áp dụng.
Ở đây mình sẽ sử dụng expected và predictions là real_stock_price, predicted_stock_price có được ở trên luôn cho tiện theo dõi nhé.

# Forecast Error (or Residual Forecast Error)

Các lỗi dự báo được tính bằng giá trị dự kiến trừ đi giá trị dự đoán.

Đây được gọi là lỗi dư của dự đoán.

>forecast_error = expected_value - predicted_value

Lỗi dự báo có thể được tính cho từng dự đoán, cung cấp một chuỗi thời gian lỗi dự báo.

Ví dụ dưới đây cho thấy cách tính lỗi dự báo cho một loạt 5 dự đoán so với 5 giá trị dự kiến. Ví dụ đã được sử dụng cho mục đích trình diễn.

![Recordit GIF](image/rfe.PNG)

# Mean Forecast Error (or Forecast Bias)

Lỗi dự báo trung bình được tính là trung bình của các giá trị lỗi dự báo.

>mean_forecast_error = mean(forecast_error)

Lỗi dự báo có thể là tích cực và tiêu cực. Điều này có nghĩa là khi tính trung bình của các giá trị này, một lỗi dự báo trung bình lý tưởng sẽ bằng không.

Giá trị lỗi dự báo trung bình khác 0 cho thấy xu hướng của mô hình quá dự báo (lỗi âm) hoặc theo dự báo (lỗi dương). Như vậy, lỗi dự báo trung bình cũng được gọi là sai lệch dự báo .

Lỗi dự báo có thể được tính trực tiếp như giá trị trung bình của các giá trị dự báo. Ví dụ dưới đây cho thấy giá trị trung bình của các lỗi dự báo có thể được tính toán bằng tay.

![Recordit GIF](image/mfe.PNG)


# Mean Absolute Error

Các sai số tuyệt đối trung bình , hoặc MAE, được tính bằng cách lấy trung bình của các giá trị lỗi dự báo, nơi mà tất cả các giá trị dự báo buộc phải là tích cực.

Buộc các giá trị là tích cực được gọi là làm cho chúng tuyệt đối. Điều này được biểu thị bằng hàm tuyệt đối abs () hoặc được hiển thị dưới dạng toán học dưới dạng hai ký tự ống xung quanh giá trị:  | value | .

>mean_absolute_error = mean( abs(forecast_error) )

Chúng ta có thể sử dụng hàm mean_absolute_error () từ thư viện scikit-learn để tính toán sai số tuyệt đối trung bình cho danh sách dự đoán. Ví dụ dưới đây thể hiện chức năng này.

![Recordit GIF](image/mae.PNG)

# Mean Squared Error

Mean Squared Error hay mse có thể dịch là lỗi bình phương trung bình được tính bằng cách lấy trung bình của các giá trị lỗi dự báo. Bình phương các giá trị lỗi dự báo buộc chúng phải dương; nó cũng có tác dụng làm tăng thêm trọng lượng cho các lỗi lớn.

Các lỗi dự báo rất lớn hoặc ngoại lệ được bình phương, do đó có tác dụng kéo giá trị trung bình của các lỗi dự báo bình phương ra dẫn đến điểm số bình phương trung bình lớn hơn. Trong thực tế, điểm số mang lại hiệu suất kém hơn cho những mô hình đưa ra dự báo sai lớn.

>mean_squared_error = mean(forecast_error^2)

Chúng ta có thể sử dụng hàm mean_squared_error () từ scikit-learn để tính toán bình phương trung bình cho một danh sách dự đoán. Ví dụ dưới đây thể hiện chức năng này.

![Recordit GIF](image/mse.PNG)

# Root Mean Squared Error

Các lỗi bình phương trung bình được mô tả ở trên là trong các đơn vị bình phương của các dự đoán.

Nó có thể được chuyển trở lại thành các đơn vị ban đầu của các dự đoán bằng cách lấy căn bậc hai của điểm lỗi bình phương trung bình. Đây được gọi là lỗi bình phương gốc , hay RMSE.

>rmse = sqrt(mean_squared_error)

Điều này có thể được tính bằng cách sử dụng hàm toán học sqrt () trên lỗi bình phương trung bình được tính bằng hàm scikit-learn mean_squared_error () .

![Recordit GIF](image/rmse.PNG)

>Warning: Be careful otherwise you will become rich


# Tổng kết

Sẽ là rất tuyệt vời nếu chúng ta tạo được model dự đoán gần đúng nhất tỉ giá của các đồng USDJPY... vì nó có thể đem đến cho chúng ta lợi nhuận cao từ việc đầu tư vào cổ phiếu, chứng khoán...
Nó sẽ không hẳn đạt 100% về độ chính xác, nhưng ít ra mình cũng đã áp dụng được AI vào việc phân tích dự đoán, và tạo được niềm tin hơn ở mỗi nến nhảy lên xuống mà chúng ta quyết đoán đánh hơn.

vậy là các bạn đã biết sử dụng LSTM để dự đoán các giá trị có tính tương quan trước sau. Trong thời gian tới mình sẽ có nhiều bài nữa liên quan đến LSTM, các bạn cùng đón xem nhé.

Nếu có khó khăn gì các bạn cứ post lên page này nhé, sẽ có nhiều cao thủ nhảy vào support ngay https://www.facebook.com/deploycai

Chúc các bạn thành công và giàu có thì nhớ đến bài hướng dẫn này của mình nhé.

Fanpage: https://www.facebook.com/deploycai

Youtube: https://www.youtube.com/channel/UCP-I4xzDwJH6E09fW85Q01A/featured?view_as=subscriber


# Tài liệu tham khảo

>https://github.com/domingos108/time_series_functions/blob/master/time_series_functions.py

>https://machinelearningmastery.com/time-series-forecasting-performance-measures-with-python/

